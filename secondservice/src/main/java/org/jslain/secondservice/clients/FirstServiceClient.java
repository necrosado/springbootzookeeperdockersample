package org.jslain.secondservice.clients;

import org.jslain.firstservice.controllers.ServiceCtrlApi;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient("firstservice")
public interface FirstServiceClient extends ServiceCtrlApi{

}
