package org.jslain.secondservice.controllers;

import java.util.List;

import org.jslain.secondservice.clients.FirstServiceClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RefreshScope
@RestController
public class ServiceCtrl {
	
	@Autowired
	private DiscoveryClient discovery;
	
	@Autowired
	private FirstServiceClient firstClient;
	
	@RequestMapping(value = "/hi", method=RequestMethod.GET, produces="text/plain")
	public String get(@RequestParam("name") String name){
		List<ServiceInstance> services = discovery.getInstances("firstservice");
		
		StringBuilder sb = new StringBuilder();
		sb.append("Remote services:").append(System.lineSeparator());
		for(ServiceInstance si : services){
			sb.append(" - ").append(si.toString()).append(System.lineSeparator());
		}
		
		sb.append(System.lineSeparator());
		sb.append("Remote service response:").append(firstClient.get(name)).append(System.lineSeparator());
		return sb.toString();
	}
}

