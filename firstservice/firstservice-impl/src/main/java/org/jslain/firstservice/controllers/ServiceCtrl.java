package org.jslain.firstservice.controllers;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ServiceCtrl implements ServiceCtrlApi{

	public String get(@RequestParam("name") String name){
		
		return "Hello " + name + "!";
	}
}


