package org.jslain.firstservice.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

public interface ServiceCtrlApi {

	@RequestMapping(value = "/hello", method=RequestMethod.GET)
	String get(@RequestParam("name") String name);
}
