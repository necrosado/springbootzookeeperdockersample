


> Written with [StackEdit](https://stackedit.io/).

This is a sample to show how 2 Spring Boot applications can be built into docker images, and establish a communicate between them the **Cloud** way using Zookeeper.

## Prerequisites ##

 - [Docker](https://www.docker.com/)
 - [JDK](http://www.oracle.com/technetwork/java/javase/downloads/index.html) (Java SDK)
 - [Maven](https://maven.apache.org/)


----------


##Installation##

First you need to install a Zookeeper Docker image in your Docker server.

    $ docker run --name some-zookeeper --restart always -d zookeeper

You can connect to your Zookeeper instance using a CLI Docker image.

    $ docker run -it --rm --link some-zookeeper:zookeeper zookeeper zkCli.sh -server zookeeper


----------

## Running the first service ##

Next you have to build and the deploy the **firstservice** application.
This service is just a simple **Hello world!**.
It starts a Tomcat container, and register a REST endpoint *dockermachine:8080/hello?name=FooBar*.

More important, it registers itself in Zookeeper so that other services can discover and call it.

    $ cd firstservice/
    $ mvn clean install
	
	$ cd firstservice-impl/
	$ mvn docker:build
    
    $ docker run --name firstservice --link some-zookeeper:zookeeper -p8080:8080 -d jslain/firstservice-impl

Up to that point, you should be able to call the hello world rest service at this URL:

    http://localhost:8080/hello?name=World

The service should give you back the string: `Hello World!`


----------

## Running the second service ##


Now you'ld be ready to build and deploy the **secondservice** application.
This service will also start a Tomcat container and publish a REST endpoint. Itwill also discover all docker machines providing the **firstservice**, list them, and call one of them.

    $ cd ../..
    $ cd secondservice/
    $ mvn clean install docker:build
    
    $ docker run --name secondservice --link some-zookeeper:zookeeper -p8081:8081 -d jslain/secondservice

And after a few seconds, you should be able to hit this URL:

    http://localhost:8081/hi?name=World


----------

## Play with them! ##

You can then start multiple others **firstservice**, to see them appear in the list when you call the second service. You can also stop them to see them disappear.

    $ docker run --name firstservice2 --link some-zookeeper:zookeeper -p8080:8080 -d jslain/firstservice-impl
    $ docker run --name firstservice3 --link some-zookeeper:zookeeper -p8080:8080 -d jslain/firstservice-impl

There's a little delay before the change actually get reflected. This is configurable in the **application.yml**. 